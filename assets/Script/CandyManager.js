const WIDTH_CANDY = 8;
const BaseCandy = require("./BaseCandy");
const NameCandy = require('./NameCandy');
cc.Class({
    extends: cc.Component,
    properties: {
        candyFb: cc.Prefab,
        boardCandy: { default: null, visible: false },
        widthCandy: { default: null, visible: false },
    },

    onLoad: function () {
        this.boardCandy = [];
        this.listNodeCandy = [];
        for (let i = 0; i < WIDTH_CANDY; i++) {
            let listCandy = [];
            for (let j = 0; j < WIDTH_CANDY; j++) {
                let candyFb = cc.instantiate(this.candyFb);
                candyFb.getComponent(BaseCandy).setup(i, j, this.boardCandy);
                this.node.addChild(candyFb);
                listCandy.push(candyFb.getComponent(BaseCandy));

                this.listNodeCandy.push(candyFb.getComponent(BaseCandy));
            }
            this.boardCandy.push(listCandy);
        }
    },
    start: function () {
        this.listNodeCandy.forEach(element => { element.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this) });
        this.listNodeCandy.forEach(element => { element.node.on(cc.Node.EventType.TOUCH_MOVE, this.onDrag, this) });
        this.listNodeCandy.forEach(element => { element.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this) });
        this.listNodeCandy.forEach(element => { element.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this) });
    },

    onTouchStart: function (event) {
        this.beginPos = event.target.getPosition();
        this.posReplace = event.target.getPosition();

        this.widthCandy = event.target.width;
        console.log("candy:" + event.target._components[0]);
        // let i = +candyNode.current.x;
        // let j = +candyNode.current.y;

        // console.log("cell: ", i, j);
        // console.log("beginPos: ", this.beginPos);

    },
    onDrag: function (event) {
        let candyNode = event.target;
        var delta = event.touch.getDelta();
        this.posReplace.x += delta.x;
        this.posReplace.y += delta.y;
        // candyNode.zIndex = 10;

        // if (candyNode.x >= this.beginPos.x + this.widthCandy) {
        //     candyNode.x = this.beginPos.x + this.widthCandy;
        // }
        // let idValidMove = this.checkMatch(this.beginPos, this.posReplace, this.widthCandy * 2);
        // this.dragCandy(idValidMove, candyNode);

        // console.log("beginPos-drag: ", event.touch.getDelta());

    },


    onTouchEnd: function (event) {
        let candyNode = event.target._components[0];
        // let posTouch = event.target.getPosition();
        let i = +candyNode.current.x;
        let j = +candyNode.current.y;
        let widthCandy = event.target.width;
        // candyNode.setPosCandy(i, j);
        // this.checkMatch(i, j, posTouch);
        // event.target.zIndex = 1;
        // if(!this.isMove(event.target)) return;'
        let direction = this.checkMatch(this.beginPos, this.posReplace, widthCandy);
        // console.log("direction", direction);

        this.moveCandy(direction, cc.v2(i, j));

        this.checkColForThree();
        this.checkRowForThree();
    },

    onTouchCancel: function (event) {
        // console.log("cancel: ", event.target.x);
        this.onTouchEnd(event);
    },

    // checkMatch: function (i, j, touch) {
    //     console.log("cell valids ---------------: ", i, j);
    //     let validMoves = [cc.v2(i - 1, j), cc.v2(i + 1, j), cc.v2(i, j - 1), cc.v2(i, j + 1)];
    //     for (let i = 0; i < validMoves.length; i++) {
    //         let cell = validMoves[i];
    //         let candy = this.boardCandy[cell.x][cell.y];
    //         if (this.checkPositionMatch(candy.node, touch)){
    //             console.log("cell Valid : ", candy);
    //             break;
    //         }
    //     }
    // },

    checkMatch: function (posBegin, posReplace, width) {
        let delX = Math.abs(posReplace.x - posBegin.x);
        let delY = Math.abs(posReplace.y - posBegin.y);

        if (delX <= width / 2 && delY <= width / 2) return "NULL";

        if (delX > delY) {
            if ((posBegin.x + width / 2) <= posReplace.x && (posBegin.x + width / 2) > posBegin.x) return "RIGHT";
            else return "LEFT";
        }
        else {
            if ((posBegin.y + width / 2) <= posReplace.y && (posBegin.y + width / 2) > posBegin.y) return "UP";
            else return "BOTTOM";
        }
    },

    moveCandy: function (nameMove, cell) {
        let beginColor = this.boardCandy[cell.x][cell.y].nameCandy;
        let replaceColor = '';
        switch (nameMove) {
            case "RIGHT": {
                replaceColor = this.boardCandy[cell.x][cell.y + 1].nameCandy;
                this.boardCandy[cell.x][cell.y].setImgCandy(replaceColor);
                this.boardCandy[cell.x][cell.y + 1].setImgCandy(beginColor);
                break;
            }
            case "LEFT": {
                replaceColor = this.boardCandy[cell.x][cell.y - 1].nameCandy;
                this.boardCandy[cell.x][cell.y].setImgCandy(replaceColor);
                this.boardCandy[cell.x][cell.y - 1].setImgCandy(beginColor);
                break;
            }
            case "UP": {
                replaceColor = this.boardCandy[cell.x + 1][cell.y].nameCandy;
                this.boardCandy[cell.x][cell.y].setImgCandy(replaceColor);
                this.boardCandy[cell.x + 1][cell.y].setImgCandy(beginColor);
                break;
            }
            case "BOTTOM": {
                replaceColor = this.boardCandy[cell.x - 1][cell.y].nameCandy;
                this.boardCandy[cell.x][cell.y].setImgCandy(replaceColor);
                this.boardCandy[cell.x - 1][cell.y].setImgCandy(beginColor);
                break;
            }
            default: {
                this.boardCandy[cell.x][cell.y].setImgCandy(beginColor);
            }
        }
    },

    dragCandy: function (nameMove, candyNode) {
        switch (nameMove) {
            case "RIGHT": {
                candyNode.x = this.beginPos.x + this.widthCandy;
                candyNode.y = this.beginPos.y;
                break;
            }
            case "LEFT": {
                candyNode.x = this.beginPos.x - this.widthCandy;
                candyNode.y = this.beginPos.y;
                break;
            }
            case "UP": {
                candyNode.y = this.beginPos.y + this.widthCandy;
                candyNode.x = this.beginPos.x;
                break;
            }
            case "BOTTOM": {
                candyNode.y = this.beginPos.y - this.widthCandy;
                candyNode.x = this.beginPos.x;
                break;
            }
        }
    },

    checkRowForThree: function () {
        // 0 1 2 3 4 5 6 7
        for (let i = 0; i < WIDTH_CANDY; i++) {
            for (let j = 0; j < WIDTH_CANDY; j++) {
                if (j > 5) continue;

                let rowOfThree = [j, j + 1, j + 2];
                let decidedColor = this.boardCandy[i][j].nameCandy;

                if (rowOfThree.every(e => this.boardCandy[i][e].nameCandy === decidedColor)) {

                    for (let e = 0; e < rowOfThree.length; e++) {
                        let candy = this.boardCandy[i][rowOfThree[e]];
                        this.effectCandy(candy);
                    }
                }
            }
        }
    },
    checkColForThree: function () {
        for (let i = 0; i < WIDTH_CANDY; i++) {
            if (i > 5) break;
            for (let j = 0; j < WIDTH_CANDY; j++) {

                let colOfThree = [i, i + 1, i + 2];
                let decidedColor = this.boardCandy[i][j].nameCandy;

                if (colOfThree.every(e => this.boardCandy[e][j].nameCandy === decidedColor)) {

                    for (let e = 0; e < colOfThree.length; e++) {
                        let candy = this.boardCandy[colOfThree[e]][j];
                        this.effectCandy(candy);
                    }
                }
            }
        }
    },


    effectRowCandy: function (i, listMatch) {
        for (let e = 0; e < listMatch.length; e++) {
            let candy = this.boardCandy[i][listMatch[e]];
            cc.tween(candy.node)
                .to(.1, { scale: 1.3 })
                .to(.2, { scale: 0 })
                .call(() => { candy.setImgCandy('') })
                .start()
        }
    },
    effectCandy: function (candy) {
        cc.tween(candy.node)
            .to(.1, { scale: 1.3 })
            .to(.2, { scale: 0 })
            .call(() => { candy.setImgCandy('') })
            .start()
    }

});