const NameCandy = cc.Enum({
    BLUE: 'blue-candy',
    GREEN: 'green-candy',
    ORANGE: 'orange-candy',
    PURPLE: 'purple-candy',
    RED: 'red-candy',
    YELLOW: 'yellow-candy'
});

module.exports = NameCandy;