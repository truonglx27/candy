const SCREEN_WIDTH = 800;
const SCREEN_HEIGHT = 800;
const NameCandy = require('./NameCandy');
let ColorCandy = [NameCandy.BLUE, NameCandy.GREEN, NameCandy.ORANGE, NameCandy.PURPLE, NameCandy.RED, NameCandy.YELLOW];
cc.Class({
    extends: cc.Component,
    properties: {
        pos: { default: null, type: cc.v2, visible: false },
        nameCandy: { default: null, visible: false },
        current: { default: null, type: cc.v2, visible: false },
        board: { default: null, type: cc.v2, visible: false },

        beginColor: { default: null, visible: false },
        beginIndex: { default: null, visible: false },
        replaceColor: { default: null, visible: false },
        replaceIndex: { default: null, visible: false },
    },

    start: function () {
        // this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        // this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onDrag, this);
        // this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        // this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    },

    setImgCandy: function (nameCandy) {
        this.nameCandy = nameCandy;
        let imgCandy = this.node.getChildren()[0];
        cc.loader.loadRes('Image/' + nameCandy, cc.SpriteFrame, function (err, sprite) {
            imgCandy.getComponent(cc.Sprite).spriteFrame = sprite;
            imgCandy.setScale(cc.v2(1, 1));
        });
    },
    setPosCandy: function (i, j) {
        this.pos = this.getPosCandy(i, j);
        this.node.setPosition(this.pos);
    },

    getPosCandy: function (i, j) {
        let width = this.node.width;
        return cc.v2(-SCREEN_WIDTH / 2 + width / 2 + width * j, -SCREEN_HEIGHT / 2 + width / 2 + width * i);
    },

    setup: function (i, j, board) {
        let randomColor = Math.floor(Math.random() * ColorCandy.length);
        this.nameCandy = ColorCandy[randomColor];
        this.setImgCandy(this.nameCandy);
        this.setPosCandy(i, j);
        this.current = cc.v2(i, j);
        this.board = board;
    },

    onTouchStart: function (event) {
        this.beginColor = this.nameCandy;
        this.beginIndex = this.current;
        console.log("touch start : ", event);
    },

    onDrag: function (event) {
        var delta = event.touch.getDelta();
        this.node.x += delta.x;
        this.node.y += delta.y;
        this.node.zIndex = 10;
    },

    onTouchEnd: function (event) {
        // this.replaceColor = 
        let i = +this.current.x;
        let j = +this.current.y;

        let validMoves = [cc.v2(i - 1, j), cc.v2(i + 1, j), cc.v2(i, j - 1), cc.v2(i, j + 1)];

        this.setPosCandy(this.current.x, this.current.y);
        this.node.zIndex = 1;

        console.log("touch end : ", event);
    },
    onTouchCancel:function(event){
        console.log("touch cancel : ", event);
    },
    getChild: function () {
        return this.node.getChildren()[0];
    },

    

});

